import Vue from 'vue';
import VueRouter from 'vue-router';
import DetailPage from '~/views/DetailPage.vue';

Vue.use(VueRouter);

export const options = {
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/pokemon/:name',
      component: DetailPage,
      props: true
    }
  ]
};
export default new VueRouter(options);
